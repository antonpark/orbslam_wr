#ifndef FEATURE_EXTRACTOR_H_
#define FEATURE_EXTRACTOR_H_

#include <memory>

#include <opencv2/opencv.hpp>

#include "feature_point_3d.h"
#include "orb_extractor.h"

namespace orbslam_wr {

class FeatureExtractor {
  public:
    explicit FeatureExtractor(int feature_size, float scale_factor, int levels,
                 int ini_th_FAST, int min_th_FAST);

    void ExtractFeaturePoints3D(cv::InputArray img, cv::InputArray mask,
            cv::Mat& depth,
            std::vector<std::shared_ptr<FeaturePoint3D>>& points);

    std::vector<float> inline GetScaleFactors(){
        return orb_extractor_.GetScaleFactors();
    }

    std::vector<float> inline GetInverseScaleSigmaSquares(){
        return orb_extractor_.GetInverseScaleSigmaSquares();
    }

  private:
    ORB_SLAM2::ORBextractor orb_extractor_;

};

} //namespace orbslam_wr

#endif //FEATURE_EXTRACTOR_H_

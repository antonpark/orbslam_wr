#ifndef FEATURE_POINT_3D_H_
#define FEATURE_POINT_3D_H_

#include <opencv2/opencv.hpp>

namespace orbslam_wr {

struct FeaturePoint3D {
  FeaturePoint3D() : keypoint(cv::KeyPoint()), disparity_x(0.0f),
                     depth(-1.0f),
                     desc(cv::Mat()), pos_world(cv::Mat()) {}

  cv::KeyPoint keypoint;
  cv::KeyPoint undist_keypoint;
  float disparity_x;
  float depth;
  cv::Mat desc;
  cv::Mat pos_world;
};

}//namespace orbslam_wr

#endif //FEATURE_POINT_3D_H_

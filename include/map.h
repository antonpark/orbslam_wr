#ifndef MAP_H_
#define MAP_H_

#include <memory>

#include "map_point.h"

namespace orbslam_wr {

class Map {
  public:
    void AddMapPoint(std::shared_ptr<MapPoint>& map_point);
    void Reset();

    std::vector<std::shared_ptr<MapPoint>>& map_points() {
      return map_points_;
    }

  private:
    std::vector<std::shared_ptr<MapPoint>> map_points_;
    static int map_point_number_;

};

}

#endif //MAP_H_

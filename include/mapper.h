#ifndef MAPPER_H_
#define MAPPER_H_

#include <memory>
#include <mutex>
#include <queue>
#include <thread>

#include "feature_matcher.h"
#include "frame.h"
#include "graph_optimizer.h"
#include "macros.h"
#include "map.h"

namespace orbslam_wr {

class Mapper {
  public:
    Mapper();
    ~Mapper();

    void Start();
    void Stop();

    void AddKeyFrame(std::shared_ptr<Frame>& frame);
    void Reset();

    std::vector<std::shared_ptr<Frame>>& keyframes() { return keyframes_; }
    Map& map() { return map_; }

    std::mutex& map_data_lock() { return map_data_lock_; }

  private:
    void Run();
    void AddMapPointsInFrame(std::shared_ptr<Frame>& frame);
    void ReloadKeyFrames();

    std::vector<std::shared_ptr<Frame>> keyframes_;
    std::queue<std::shared_ptr<Frame>> incoming_keyframes_queue_;
    std::queue<std::shared_ptr<Frame>> keyframes_queue_;
    Map map_;

    std::thread thread_;
    std::mutex incoming_kf_queue_lock_;
    std::mutex map_data_lock_;

    GraphOptimizer graph_optimizer_;

    FeatureMatcher feature_matcher_;

    bool keep_running_;

    DISALLOW_COPY_AND_ASSIGN(Mapper);
};

} //namespace orbslam_wr

#endif //MAPPER_H_

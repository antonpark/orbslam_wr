#ifndef MAP_POINT_H_
#define MAP_POINT_H_

#include <memory>

#include <opencv2/core/core.hpp>

#include "feature_point_3d.h"
#include "frame_interface.h"

namespace orbslam_wr {

class MapPoint {
  public:
    MapPoint();
    ~MapPoint();

    void AddObserver(FrameInterface* frame);
    std::vector<FrameInterface*>& observer() { return observers_; }

    cv::Mat pos_world;
    cv::Mat normal;
    cv::Mat desc;

    bool is_mapped;
    int id;
    int tracking_score;

  private:
    std::vector<FrameInterface*> observers_;
};

} //namespace orbslam_wr

#endif //MAP_POINT_H_

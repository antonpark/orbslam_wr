#ifndef FRAME_H_
#define FRAME_H_

#include <memory>

#include <opencv2/opencv.hpp>

#include "feature_extractor.h"
#include "feature_point_3d.h"
#include "frame_interface.h"
#include "map_point.h"
#include "orb_vocabulary.h"

namespace orbslam_wr {

class Frame : public FrameInterface {
  public:
    explicit Frame(cv::Mat& rgb_img, cv::Mat& depth_img,
          FeatureExtractor* feature_extractor,
          ORBVocabulary* orb_vocabulary);

    virtual ~Frame() override;

    std::vector<std::shared_ptr<FeaturePoint3D>>& feature_points() {
      return feature_points_;
    }

    int feature_points_size() { return feature_points_size_; }

    std::vector<std::shared_ptr<MapPoint>>& map_points() { return map_points_; }

    int map_point_match_size;

    cv::Mat FeatureToWorldPose(int idx);
    void UpdateFeaturesWorldPose();
    void UpdatePose();
    void SetPose(cv::Mat& pose);

    void TransformDescToBow();

    struct CameraParam {
      float fx;
      float fy;
      float cx;
      float cy;
      float bf;
      cv::Mat dist_coef;

      float image_bound_x;
      float image_bound_y;
    };

    static void SetCameraParam(CameraParam& param);
    FeatureExtractor* GetFeatureExtractor() { return feature_extractor_; }

    cv::Mat& GetFramePose() { return Tcw_; }
    void SetReferenceFrame(std::shared_ptr<Frame>& frame);
    std::shared_ptr<Frame>& ref_frame() { return ref_frame_; }

    bool IsPointInImageBound(cv::Mat point);

    static float fx() { return fx_; }
    static float fy() { return fy_; }
    static float cx() { return cx_; }
    static float cy() { return cy_; }
    static float bf() { return bf_; }
    static cv::Mat& dist_coef() { return dist_coef_; }
    static cv::Mat& K() { return K_; }

    cv::Mat& Tcw() { return Tcw_; }
    cv::Mat& Rcw() { return Rcw_; }
    cv::Mat& Rwc() { return Rwc_; }
    cv::Mat& tcw() { return tcw_; }
    cv::Mat& twc() { return twc_; }
    cv::Mat& Twc() { return Twc_; }

    cv::Mat& Tcr() { return Tcr_; }

    static uint64_t global_frame_number;
    uint64_t id;

  private:
    std::shared_ptr<Frame> ref_frame_;
    std::vector<std::shared_ptr<FeaturePoint3D>> feature_points_;
    int feature_points_size_;

    FeatureExtractor* feature_extractor_;
    ORBVocabulary* orb_vocabulary_;

    DBoW2::BowVector BoW_vector_;
    DBoW2::FeatureVector BoW_Feature_vector_;

    std::vector<std::shared_ptr<MapPoint>> map_points_;

    cv::Mat Tcw_;
    cv::Mat Twc_;
    cv::Mat Rcw_;
    cv::Mat Rwc_;
    cv::Mat tcw_;
    cv::Mat twc_;

    //Pose relative to the reference frame
    cv::Mat Tcr_;

    static float fx_;
    static float fy_;
    static float cx_;
    static float cy_;
    static float fx_inv_;
    static float fy_inv_;
    static float bf_;
    static cv::Mat dist_coef_;
    static cv::Mat K_;

    static float img_bound_x_max_;
    static float img_bound_x_min_;
    static float img_bound_y_max_;
    static float img_bound_y_min_;
};

} //namespace orbslam_wr


#endif //FRAME_H_

#ifndef SLAM_SYSTEM_H_
#define SLAM_SYSTEM_H_

#include <iostream>

#include <opencv2/core/core.hpp>

#include "feature_extractor.h"
#include "tracker.h"
#include "macros.h"
#include "orb_vocabulary.h"

namespace orbslam_wr {

class SLAMSystem {
  public:
    SLAMSystem();

    void LoadSettingsFile(std::string file_path);
    void LoadORBVocabularyFile(std::string file_path);

    void InitializeSystem();

    void SendRGBDImage(cv::Mat& rgb_img, cv::Mat& depth_img);

    //DEBUG
    Frame* GetCurrentFrame() { return tracker_.current_frame(); }
    Mapper& GetMapper() { return tracker_.mapper(); }

  private:
    Tracker tracker_;
    FeatureExtractor feature_extractor_;
    ORBVocabulary orb_vocabulary_;

    cv::FileStorage yaml_settings_;

    float depth_map_factor_;
    float depth_map_factor_inv_;

    bool tracker_initialized;

    DISALLOW_COPY_AND_ASSIGN(SLAMSystem);
};

} //namespace orbslam_wr

#endif //SLAM_SYSTEM_H_

#ifndef GRAPH_OPTIMIZER_H_
#define GRAPH_OPTIMIZER_H_

#include "frame.h"
#include "map.h"

namespace orbslam_wr {

class GraphOptimizer {
  public:
    GraphOptimizer();
    int PoseOnlyOptimize(Frame* frame);
    void BundleAdjustment(std::vector<std::shared_ptr<Frame>>& keyframes,
                          Map& map,
                          std::vector<cv::Mat>& out_poses,
                          std::vector<cv::Mat>& out_map_points);

  private:

};

}

#endif //GRAPH_OPTIMIZER_H_

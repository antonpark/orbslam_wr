#ifndef FRAME_INTERFACE_H_
#define FRAME_INTERFACE_H_

namespace orbslam_wr {

class FrameInterface {
  public:
    virtual ~FrameInterface() {}

};

} //namespace orbslam_wr

#endif //FRAME_INTERFACE_H_

#ifndef FEATURE_MATCHER_H_
#define FEATURE_MATCHER_H_

#include "frame.h"
#include "map.h"

namespace orbslam_wr {

class FeatureMatcher {
  public:
    struct WindowSize {
      WindowSize(float x, float y) : width(x), height(y) {}

      float width;
      float height;
    };

    int FrameToFrameORBMatch(Frame* frame1, Frame* frame2,
                                    WindowSize& size);

    int RefToFrameORBMatch(Frame* frame1, Frame* frame2,
                                    WindowSize& size);

    int KeyframeToKeyframeORBMatch(Frame* keyframe1, Frame* keyframe2);

    // Computes the Hamming distance between two ORB descriptors
    int DescriptorDistance(const cv::Mat &a, const cv::Mat &b);

  private:
    void ComputeThreeMaxima(std::vector<int>* histo, const int L, int &ind1, int &ind2, int &ind3);

};

}

#endif //FEATURE_MATCHER_H_

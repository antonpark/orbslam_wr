#ifndef TRACKER_H_
#define TRACKER_H_

#include <memory>

#include "feature_matcher.h"
#include "frame.h"
#include "graph_optimizer.h"
#include "macros.h"
#include "map.h"
#include "mapper.h"

namespace orbslam_wr {

class Tracker {
  public:
    Tracker();
    ~Tracker();

    bool Init(Frame* frame);
    bool Track(Frame* frame);

    Frame* current_frame() { return current_frame_.get(); }
    Mapper& mapper() { return mapper_; }

  private:
    void EstimateVelocity();

    GraphOptimizer graph_optimizer_;
    std::shared_ptr<Frame> current_frame_;
    std::shared_ptr<Frame> last_frame_;
    std::shared_ptr<Frame> ref_frame_;
    Mapper mapper_;

    FeatureMatcher feature_matcher_;

    cv::Mat velocity_;

    Map map_;

    DISALLOW_COPY_AND_ASSIGN(Tracker);
};

} //namespace orbslam_wr

#endif //TRACKER_H_

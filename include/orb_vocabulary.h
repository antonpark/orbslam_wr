
#ifndef ORBVOCABULARY_H_
#define ORBVOCABULARY_H_

#include "3rdParty/DBoW2/DBoW2/FORB.h"
#include "3rdParty/DBoW2/DBoW2/TemplatedVocabulary.h"

namespace orbslam_wr {

typedef DBoW2::TemplatedVocabulary<DBoW2::FORB::TDescriptor, DBoW2::FORB>
  ORBVocabulary;

} // namespace orbslam_wr

#endif //ORBVOCABULARY_H_

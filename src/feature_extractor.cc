#include "feature_extractor.h"
#include "frame.h"

namespace orbslam_wr {

FeatureExtractor::FeatureExtractor(int feature_size,
                                   float scale_factor, int levels,
                                   int ini_th_FAST, int min_th_FAST)
  : orb_extractor_(feature_size,
                   scale_factor, levels,
                   ini_th_FAST, min_th_FAST) {

}

void FeatureExtractor::ExtractFeaturePoints3D(
  cv::InputArray img, cv::InputArray mask,
  cv::Mat& depth,
  std::vector<std::shared_ptr<FeaturePoint3D>>& points) {

  //FIX ME: optimize extraction process
  std::vector<cv::KeyPoint> keys;
  cv::Mat descriptors;

  orb_extractor_(img, mask, keys, descriptors);

  int keys_size = keys.size();

  if (keys_size == 0) {
    return;
  }

  points.resize(keys_size);

  //Extract key points
  for (int i = 0 ; i < keys_size; i++) {
    points[i] = std::make_shared<FeaturePoint3D>();
    points[i]->keypoint = keys[i];
    points[i]->desc = descriptors.row(i);
  }

  //Calculate undistorted key points
  cv::Mat mat(keys_size, 2, CV_32F);

  for (int i = 0; i < keys_size; i++) {
    mat.at<float>(i, 0) = points[i]->keypoint.pt.x;
    mat.at<float>(i, 1) = points[i]->keypoint.pt.y;
  }

  mat = mat.reshape(2);
  cv::undistortPoints(mat, mat,
                      Frame::K(), Frame::dist_coef(),
                      cv::Mat(), Frame::K());
  mat = mat.reshape(1);

  for (int i = 0; i < keys_size; i++) {
    points[i]->undist_keypoint.pt.x = mat.at<float>(i, 0);
    points[i]->undist_keypoint.pt.y = mat.at<float>(i, 1);
  }

  //Calculate virtual stereo disparity (for stereo optimization)
  for (int i = 0; i < keys_size; i++) {
    float d = depth.at<float>(
        static_cast<int>(points[i]->keypoint.pt.y),
        static_cast<int>(points[i]->keypoint.pt.x)
      );

    if (d > 0) {
      points[i]->depth = d;
      points[i]->disparity_x = points[i]->undist_keypoint.pt.x - Frame::bf()/d;
    }
  }
}

} //namespace orbslam_wr

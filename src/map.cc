#include "map.h"

namespace orbslam_wr {

int Map::map_point_number_ = 0;

void Map::AddMapPoint(std::shared_ptr<MapPoint>& map_point) {
  if (map_point->is_mapped)
    return;

  map_point->is_mapped = true;
  map_point->id = map_point_number_++;
  map_points_.push_back(map_point);
}

void Map::Reset() {
  map_points_.clear();
  map_point_number_ = 0;
}

} //orbslam_wr

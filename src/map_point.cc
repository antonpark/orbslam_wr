#include "map_point.h"

#include "frame_interface.h"

namespace orbslam_wr {

MapPoint::MapPoint() : pos_world(cv::Mat()),
    normal(cv::Mat()), desc(cv::Mat()),
    is_mapped(false), id(-1), tracking_score(0) {

}

MapPoint::~MapPoint() {

}

void MapPoint::AddObserver(FrameInterface* frame) {
  std::vector<FrameInterface*>::iterator it
    = std::find(observers_.begin(), observers_.end(), frame);

  if (it == observers_.end())
    observers_.push_back(frame);
}


} //namespace orbslam_wr

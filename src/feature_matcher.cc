#include "feature_matcher.h"

namespace orbslam_wr {

const float kHistLength = 30.0f;
const float kHistFactor = 1.0f/kHistLength;

cv::Mat SkewSymmetricMatrix(const cv::Mat &mat) {
   return (cv::Mat_<float>(3,3) <<
           0, -mat.at<float>(2), mat.at<float>(1),
           mat.at<float>(2), 0, -mat.at<float>(0),
           -mat.at<float>(1), mat.at<float>(0), 0);
}

bool CheckDistEpipolarLine(const cv::KeyPoint &kp1,const cv::KeyPoint &kp2,
                           const cv::Mat &F) {
    // Epipolar line in second image l = x1'F = [a b c]
    const float a = kp1.pt.x*F.at<float>(0,0)+kp1.pt.y*F.at<float>(1,0)+F.at<float>(2,0);
    const float b = kp1.pt.x*F.at<float>(0,1)+kp1.pt.y*F.at<float>(1,1)+F.at<float>(2,1);
    const float c = kp1.pt.x*F.at<float>(0,2)+kp1.pt.y*F.at<float>(1,2)+F.at<float>(2,2);

    const float num = a*kp2.pt.x+b*kp2.pt.y+c;

    const float den = a*a+b*b;

    if(den==0)
        return false;

    const float dsqr = num*num/den;

    return dsqr < 10;
}

int FeatureMatcher::FrameToFrameORBMatch(Frame* frame1, Frame* frame2,
                                         WindowSize& win_size) {
  int feature_size_1 = frame1->feature_points_size();
  int feature_size_2 = frame2->feature_points_size();

  std::vector<std::shared_ptr<FeaturePoint3D>>& feature_points_1
    = frame1->feature_points();
  std::vector<std::shared_ptr<FeaturePoint3D>>& feature_points_2
    = frame2->feature_points();

  std::vector<std::shared_ptr<MapPoint>>& map_points_1
    = frame1->map_points();
  std::vector<std::shared_ptr<MapPoint>>& map_points_2
    = frame2->map_points();

  std::vector<float> scale_factors
    = frame1->GetFeatureExtractor()->GetScaleFactors();

  cv::Mat Rcw = frame2->GetFramePose().rowRange(0, 3).colRange(0, 3);
  cv::Mat tcw = frame2->GetFramePose().rowRange(0, 3).col(3);

  std::vector<int> rot_hist[static_cast<int>(kHistLength)];
  for (int i = 0; i < static_cast<int>(kHistLength); i++) {
    rot_hist[i].reserve(500);
  }

  int matched_count = 0;

  for (int i = 0; i < feature_size_1; i++) {
    cv::Mat pos_world;

    /*if (map_points_1[i]) {
      pos_world = map_points_1[i]->pos_world;
    }
    else {
      pos_world = feature_points_1[i]->pos_world;
    }
    */
    pos_world = feature_points_1[i]->pos_world;

    if (pos_world.empty()) {
      continue;
    }

    cv::Mat pos_projected = Rcw * pos_world + tcw;

    if (pos_projected.at<float>(2) < 0.0f) {
      //printf("points out of depth\n");
      continue;
    }

    if (!frame2->IsPointInImageBound(pos_projected)) {
      //printf("points not in image bound\n");
      continue;
    }

    int dist_best = 256;
    int idx_best = -1;
    for (int j = 0 ; j < feature_size_2; j++) {
      int x_diff = feature_points_1[i]->keypoint.pt.x
                   - feature_points_2[j]->keypoint.pt.x;
      int y_diff = feature_points_1[i]->keypoint.pt.y
                   - feature_points_2[j]->keypoint.pt.y;

      float win_scale = scale_factors[feature_points_2[j]->keypoint.octave];

      if (abs(x_diff) > win_scale * win_size.width
          || abs(y_diff) > win_scale * win_size.height) {
        continue;
      }

      //FIX ME: if map point descriptor is multiple or normalized,it must be counted
      int dist = DescriptorDistance(feature_points_1[i]->desc,
                                    feature_points_2[j]->desc);

      if (dist < dist_best) {
        dist_best = dist;
        idx_best = j;
      }
    }

    if (dist_best <= 100) {
      /*if (map_points_1[i] && map_points_2[idx_best] != map_points_1[i]) {
        map_points_2[idx_best] = map_points_1[i];
        ++(map_points_2[idx_best]->tracking_score);
      }*/

      feature_points_2[idx_best]->pos_world = pos_world;

      //printf("feature matched: %d\n", dist_best);
      matched_count++;

      //feature orientation check
      float angle_diff = feature_points_1[i]->undist_keypoint.angle
                         - feature_points_2[idx_best]->undist_keypoint.angle;

      if (angle_diff < 0.0f) {
        angle_diff += 360.0f;
      }

      int bin = round(angle_diff * kHistFactor);

      assert(bin >= 0 && bin < kHistLength);

      rot_hist[bin].push_back(idx_best);
    }
  }

  //feature orientation check
  int idx_1 = -1;
  int idx_2 = -1;
  int idx_3 = -1;

  ComputeThreeMaxima(rot_hist, kHistLength, idx_1, idx_2, idx_3);

  for (int i = 0; i < kHistLength; i++) {
    if (i != idx_1 && i != idx_2 && i != idx_3) {
      for (size_t j = 0, j_end = rot_hist[i].size(); j < j_end; j++) {
        /*if (map_points_2[rot_hist[i][j]]) {
          --(map_points_2[rot_hist[i][j]]->tracking_score);
          map_points_2[rot_hist[i][j]].reset();
        }*/
        feature_points_2[rot_hist[i][j]]->pos_world = cv::Mat();
        matched_count--;
      }
    }
  }

  return matched_count;
}

int FeatureMatcher::RefToFrameORBMatch(Frame* frame1, Frame* frame2,
                                    WindowSize& win_size) {
  int feature_size_1 = frame1->feature_points_size();
  int feature_size_2 = frame2->feature_points_size();

  std::vector<std::shared_ptr<FeaturePoint3D>>& feature_points_1
    = frame1->feature_points();
  std::vector<std::shared_ptr<FeaturePoint3D>>& feature_points_2
    = frame2->feature_points();

  std::vector<std::shared_ptr<MapPoint>>& map_points_1
    = frame1->map_points();
  std::vector<std::shared_ptr<MapPoint>>& map_points_2
    = frame2->map_points();

  std::vector<float> scale_factors
    = frame1->GetFeatureExtractor()->GetScaleFactors();

  cv::Mat Rcw = frame2->GetFramePose().rowRange(0, 3).colRange(0, 3);
  cv::Mat tcw = frame2->GetFramePose().rowRange(0, 3).col(3);

  std::vector<int> rot_hist[static_cast<int>(kHistLength)];
  for (int i = 0; i < static_cast<int>(kHistLength); i++) {
    rot_hist[i].reserve(500);
  }

  int matched_count = 0;

  for (int i = 0; i < feature_size_1; i++) {
    cv::Mat pos_world;

    if (map_points_1[i] && map_points_1[i]->is_mapped) {
      pos_world = map_points_1[i]->pos_world;
    }
    else {
      pos_world = feature_points_1[i]->pos_world;
    }

    if (pos_world.empty()) {
      continue;
    }

    cv::Mat pos_projected = Rcw * pos_world + tcw;

    if (pos_projected.at<float>(2) < 0.0f) {
      //printf("points out of depth\n");
      continue;
    }

    if (!frame2->IsPointInImageBound(pos_projected)) {
      //printf("points not in image bound\n");
      continue;
    }

    int dist_best = 256;
    int idx_best = -1;
    for (int j = 0 ; j < feature_size_2; j++) {
      int x_diff = feature_points_1[i]->keypoint.pt.x
                   - feature_points_2[j]->keypoint.pt.x;
      int y_diff = feature_points_1[i]->keypoint.pt.y
                   - feature_points_2[j]->keypoint.pt.y;

      float win_scale = scale_factors[feature_points_2[j]->keypoint.octave];

      if (abs(x_diff) > win_scale * win_size.width
          || abs(y_diff) > win_scale * win_size.height) {
        continue;
      }

      int dist = DescriptorDistance(feature_points_1[i]->desc,
                                    feature_points_2[j]->desc);

      if (dist < dist_best) {
        dist_best = dist;
        idx_best = j;
      }
    }

    if (dist_best <= 100) {
      if (map_points_1[i]) {
        if (!map_points_2[idx_best]) {
          map_points_2[idx_best] = map_points_1[i];
        }

        ++frame2->map_point_match_size;
        ++map_points_1[i]->tracking_score;
      }

      //feature_points_2[idx_best]->pos_world = pos_world;

      //printf("feature matched: %d\n", dist_best);
      ++matched_count;

      //feature orientation check
      float angle_diff = feature_points_1[i]->undist_keypoint.angle
                         - feature_points_2[idx_best]->undist_keypoint.angle;

      if (angle_diff < 0.0f) {
        angle_diff += 360.0f;
      }

      int bin = round(angle_diff * kHistFactor);

      assert(bin >= 0 && bin < kHistLength);

      rot_hist[bin].push_back(idx_best);
    }
  }

  //feature orientation check
  int idx_1 = -1;
  int idx_2 = -1;
  int idx_3 = -1;

  ComputeThreeMaxima(rot_hist, kHistLength, idx_1, idx_2, idx_3);

  for (int i = 0; i < kHistLength; i++) {
    if (i != idx_1 && i != idx_2 && i != idx_3) {
      for (size_t j = 0, j_end = rot_hist[i].size(); j < j_end; j++) {
        if (map_points_2[rot_hist[i][j]]) {
          --(map_points_2[rot_hist[i][j]]->tracking_score);
          --frame2->map_point_match_size;
          map_points_2[rot_hist[i][j]].reset();
        }
        //feature_points_2[rot_hist[i][j]]->pos_world = cv::Mat();
        --matched_count;
      }
    }
  }

  return matched_count;
}

int FeatureMatcher::KeyframeToKeyframeORBMatch(Frame* keyframe1, Frame* keyframe2) {
  //iterate through keypoints
  //match
  //if at least one of matched kp has world point, take it
  //else both is monocular obs, do linear triangulation method
  //make map point

  int feature_size_1 = keyframe1->feature_points_size();
  int feature_size_2 = keyframe2->feature_points_size();

  std::vector<std::shared_ptr<FeaturePoint3D>>& feature_points_1
    = keyframe1->feature_points();
  std::vector<std::shared_ptr<FeaturePoint3D>>& feature_points_2
    = keyframe2->feature_points();

  std::vector<std::shared_ptr<MapPoint>>& map_points_1
    = keyframe1->map_points();
  std::vector<std::shared_ptr<MapPoint>>& map_points_2
    = keyframe2->map_points();

  std::vector<float> scale_factors
    = keyframe1->GetFeatureExtractor()->GetScaleFactors();

  cv::Mat Rcw = keyframe2->GetFramePose().rowRange(0, 3).colRange(0, 3);
  cv::Mat tcw = keyframe2->GetFramePose().rowRange(0, 3).col(3);

  //epipole in keyframe2
  cv::Mat twc_1 = keyframe1->twc();
  cv::Mat twc_1_in_2 = Rcw * twc_1 + tcw;
  float epipole_x = Frame::fx() * twc_1_in_2.at<float>(0)
                    / twc_1_in_2.at<float>(2) + Frame::cx();
  float epipole_y = Frame::fy() * twc_1_in_2.at<float>(1)
                    / twc_1_in_2.at<float>(2) + Frame::cy();

  //Fundamental matrix
  cv::Mat R12 = keyframe1->Rcw() * keyframe2->Rwc();
  cv::Mat t12 = -keyframe1->Rcw() * keyframe2->Rwc() * tcw + keyframe1->tcw();
  cv::Mat t12_skew = SkewSymmetricMatrix(t12);

  cv::Mat F = Frame::K().t().inv() * t12_skew * R12 * Frame::K().inv();

  std::vector<int> rot_hist[static_cast<int>(kHistLength)];
  for (int i = 0; i < static_cast<int>(kHistLength); i++) {
    rot_hist[i].reserve(500);
  }

  int matched_count = 0;
  int triangulation_count = 0;

  for (int i = 0; i < feature_size_1; i++) {
    cv::Mat pos_world;

    if (map_points_1[i] && map_points_1[i]->is_mapped) {
      pos_world = map_points_1[i]->pos_world;
    }
    else {
      pos_world = feature_points_1[i]->pos_world;
    }

    //if (pos_world.empty()) {
    //  continue;
    //}

    if (!pos_world.empty()) {
      cv::Mat pos_projected = Rcw * pos_world + tcw;

      if (pos_projected.at<float>(2) < 0.0f) {
        //printf("points out of depth\n");
        continue;
      }

      if (!keyframe2->IsPointInImageBound(pos_projected)) {
        //printf("points not in image bound\n");
        continue;
      }
    }

    int dist_best = 256;
    int idx_best = -1;
    for (int j = 0 ; j < feature_size_2; j++) {
      float win_scale = scale_factors[feature_points_2[j]->keypoint.octave];

      if (!pos_world.empty()) {
        //check distance between kp2 and kp1 projected in frame 2
        cv::Mat pos_projected = Rcw * pos_world + tcw;

        float x_diff = pos_projected.at<float>(0)
                     - feature_points_2[j]->keypoint.pt.x;
        float y_diff = pos_projected.at<float>(1)
                     - feature_points_2[j]->keypoint.pt.y;

        if (abs(x_diff) > win_scale * 20
            || abs(y_diff) > win_scale * 20) {
          continue;
        }
      }

      int dist = DescriptorDistance(feature_points_1[i]->desc,
                                    feature_points_2[j]->desc);

      if (dist < dist_best) {
          if (pos_world.empty() && feature_points_2[j]->pos_world.empty()) {
          //if point is too close to epipole, reject
          float dist_x = epipole_x - feature_points_2[j]->keypoint.pt.x;
          float dist_y = epipole_y - feature_points_2[j]->keypoint.pt.y;
          if (dist_x * dist_x + dist_y * dist_y < 100 * win_scale) {
            continue;
          }
        }

        //if point is too far from epipolar line, reject
        if (!CheckDistEpipolarLine(feature_points_1[i]->keypoint,
                                   feature_points_2[j]->keypoint,
                                   F)) {
          continue;
        }

        dist_best = dist;
        idx_best = j;
      }
    }

    if (dist_best <= 100) {

      if (map_points_1[i]) {
        if (!map_points_2[idx_best]) {
          map_points_2[idx_best] = map_points_1[i];
        }

        ++keyframe2->map_point_match_size;
        ++map_points_1[i]->tracking_score;

        ++matched_count;
      }
      else if (!pos_world.empty() && feature_points_2[idx_best]->pos_world.empty()) {
        feature_points_2[idx_best]->pos_world = pos_world;
        ++matched_count;
      }
      else if (!feature_points_2[idx_best]->pos_world.empty()) {
        ++matched_count;
      }
      else {//monos
        cv::Mat A(4, 4, CV_32F);
        cv::Mat Tcw_1 = keyframe1->Tcw();
        cv::Mat Tcw_2 = keyframe2->Tcw();

        cv::KeyPoint kp1, kp2;
        kp1.pt.x = (feature_points_1[i]->keypoint.pt.x - Frame::cx()) / Frame::fx();
        kp1.pt.y = (feature_points_1[i]->keypoint.pt.y - Frame::cx()) / Frame::fx();
        kp2.pt.x = (feature_points_2[idx_best]->keypoint.pt.x - Frame::cx()) / Frame::fx();
        kp2.pt.y = (feature_points_2[idx_best]->keypoint.pt.y - Frame::cx()) / Frame::fx();

        A.row(0) = kp1.pt.x*Tcw_1.row(2) - Tcw_1.row(0);
        A.row(1) = kp1.pt.y*Tcw_1.row(2) - Tcw_1.row(1);
        A.row(2) = kp2.pt.x*Tcw_2.row(2) - Tcw_2.row(0);
        A.row(3) = kp2.pt.y*Tcw_2.row(2) - Tcw_2.row(1);

        cv::Mat W, U, Vt;
        cv::SVD::compute(A, W, U, Vt, cv::SVD::MODIFY_A | cv::SVD::FULL_UV);

        cv::Mat pos_world_recov = Vt.row(3).t();
        float scale_recov = pos_world_recov.at<float>(3);
        if (scale_recov == 0) {
          continue;
        }

        pos_world_recov = pos_world_recov.rowRange(0, 3)/scale_recov;

        //check pos_world
        //projected in frame1
        cv::Mat pos_projected = keyframe1->Rcw() * pos_world_recov + keyframe1->tcw();

        float x_diff = pos_projected.at<float>(0) * Frame::fx() / pos_projected.at<float>(2)
                     + Frame::cx()
                     - feature_points_1[i]->keypoint.pt.x;
        float y_diff = pos_projected.at<float>(1) * Frame::fy() / pos_projected.at<float>(2)
                     + Frame::cy()
                     - feature_points_1[i]->keypoint.pt.y;

        float win_scale = scale_factors[feature_points_1[i]->keypoint.octave];

        if (abs(x_diff) > win_scale * 20
            || abs(y_diff) > win_scale * 20) {
          continue;
        }

        //projected in frame2
        pos_projected = keyframe2->Rcw() * pos_world_recov + keyframe2->tcw();

        x_diff = pos_projected.at<float>(0) * Frame::fx() / pos_projected.at<float>(2)
                     + Frame::cx()
                     - feature_points_2[idx_best]->keypoint.pt.x;
        y_diff = pos_projected.at<float>(1) * Frame::fy() / pos_projected.at<float>(2)
                     + Frame::cy()
                     - feature_points_2[idx_best]->keypoint.pt.y;

        win_scale = scale_factors[feature_points_2[idx_best]->keypoint.octave];

        if (abs(x_diff) > win_scale * 20
            || abs(y_diff) > win_scale * 20) {
          continue;
        }

        feature_points_2[idx_best]->pos_world = pos_world_recov;
        ++matched_count;
        ++triangulation_count;
      }

      //feature orientation check
      float angle_diff = feature_points_1[i]->undist_keypoint.angle
                         - feature_points_2[idx_best]->undist_keypoint.angle;

      if (angle_diff < 0.0f) {
        angle_diff += 360.0f;
      }

      int bin = round(angle_diff * kHistFactor);

      assert(bin >= 0 && bin < kHistLength);

      rot_hist[bin].push_back(idx_best);
    }
  }

  //feature orientation check
  int idx_1 = -1;
  int idx_2 = -1;
  int idx_3 = -1;

  ComputeThreeMaxima(rot_hist, kHistLength, idx_1, idx_2, idx_3);

  for (int i = 0; i < kHistLength; i++) {
    if (i != idx_1 && i != idx_2 && i != idx_3) {
      for (size_t j = 0, j_end = rot_hist[i].size(); j < j_end; j++) {
        if (map_points_2[rot_hist[i][j]]) {
          --(map_points_2[rot_hist[i][j]]->tracking_score);
          --keyframe2->map_point_match_size;
          map_points_2[rot_hist[i][j]].reset();
        }
        //feature_points_2[rot_hist[i][j]]->pos_world = cv::Mat();
        --matched_count;
      }
    }
  }

  if (triangulation_count > 0)
    std::cout << "triangulation count: " << triangulation_count << std::endl;

  return matched_count;
}

// Bit set count operation from
// http://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetParallel
int FeatureMatcher::DescriptorDistance(const cv::Mat &a, const cv::Mat &b)
{
    const int *pa = a.ptr<int32_t>();
    const int *pb = b.ptr<int32_t>();

    int dist=0;

    for(int i=0; i<8; i++, pa++, pb++)
    {
        unsigned  int v = *pa ^ *pb;
        v = v - ((v >> 1) & 0x55555555);
        v = (v & 0x33333333) + ((v >> 2) & 0x33333333);
        dist += (((v + (v >> 4)) & 0xF0F0F0F) * 0x1010101) >> 24;
    }

    return dist;
}

void FeatureMatcher::ComputeThreeMaxima(std::vector<int>* histo, const int L, int &ind1, int &ind2, int &ind3)
{
    int max1 = 0;
    int max2 = 0;
    int max3 = 0;

    for(int i = 0; i < L; i++)
    {
        const int s = histo[i].size();
        if(s > max1)
        {
            max3 = max2;
            max2 = max1;
            max1 = s;
            ind3 = ind2;
            ind2 = ind1;
            ind1 = i;
        }
        else if(s > max2)
        {
            max3 = max2;
            max2 = s;
            ind3 = ind2;
            ind2 = i;
        }
        else if(s > max3)
        {
            max3 = s;
            ind3 = i;
        }
    }

    if(max2 < 0.1f * (float)max1)
    {
        ind2 = -1;
        ind3 = -1;
    }
    else if(max3 < 0.1f*(float)max1)
    {
        ind3 = -1;
    }
}

} //namespace orbslam_wr

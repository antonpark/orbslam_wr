#include "tracker.h"

#include <mutex>

#include "map_point.h"

namespace orbslam_wr {

Tracker::Tracker() : velocity_(cv::Mat()) {

}

Tracker::~Tracker() {
  mapper_.Stop();
}

bool Tracker::Init(Frame* frame) {
  if (frame->feature_points_size() < 100) {
    return false;
  }

  current_frame_.reset(frame);

  mapper_.Reset();

  last_frame_ = current_frame_;

  ref_frame_ = current_frame_;

  mapper_.AddKeyFrame(ref_frame_);

  return true;
}

bool Tracker::Track(Frame* frame) {
  current_frame_.reset(frame);
  int matched = 0;

  { //lock block
  //reference keyframe's pose and map point can be modified concurrently in mapper
  std::unique_lock<std::mutex> lock(mapper_.map_data_lock());
  last_frame_->UpdatePose();
  last_frame_->UpdateFeaturesWorldPose();

  //if last frame is not reference frame (key frame),
  //its pose shall not be changed during remaining tracking.
  if (last_frame_ != ref_frame_) {
   // lock.unlock();
  }

  if (velocity_.empty()) {
    current_frame_->SetPose(last_frame_->GetFramePose());
  }
  else {
    cv::Mat current_pose_estimation = velocity_ * last_frame_->GetFramePose();
    current_frame_->SetPose(current_pose_estimation);
    //printf("vel estimate %f %f %f\n", velocity_.at<float>(0, 3), velocity_.at<float>(1, 3), velocity_.at<float>(2, 3));
  }

  FeatureMatcher::WindowSize window_size(20, 20);

  //feature match
  matched = feature_matcher_.FrameToFrameORBMatch(
                  last_frame_.get(), current_frame_.get(), window_size);
  if (matched < 20) {
    printf("matching failed. %d\n", matched);
    return false;
  }

  //estimate pose by only-pose(only the vertex is optimized) graph optimization
  int count = graph_optimizer_.PoseOnlyOptimize(current_frame_.get());

  if (count < 10) {
    printf("pose optimization failed. %d\n", count);
    return false;
  }

  EstimateVelocity();

  last_frame_ = current_frame_;

  //FIX ME: Key frame insertion decision modulize
  FeatureMatcher::WindowSize window_size_ref(40, 40);

  //ref match. check map points tracked ratio
  //if (!lock.owns_lock()) {
  //  lock.lock();
  //}

  int matched_ref = feature_matcher_.RefToFrameORBMatch(
                  ref_frame_.get(), current_frame_.get(), window_size_ref);


  std::vector<std::shared_ptr<MapPoint>> map_points = ref_frame_->map_points();
  int map_point_tracked_size = 0;
  for (int i = 0; i < map_points.size(); i++) {
    if (!map_points[i] || !map_points[i]->is_mapped)
      continue;
    if (map_points[i]->tracking_score > 0) {
      map_point_tracked_size++;
    }
  }

  //std::cout << "current map point match size: "  << current_frame()->map_point_match_size <<
  //  ", tracked map point ref: " << map_point_tracked_size << ", match f2f: " << matched << ", match r2f: "<< matched_ref << std::endl;

  if (/*current_frame_->id - ref_frame_->id > 20
      || */(float)current_frame()->map_point_match_size / (float)map_point_tracked_size < 0.75) {
    ref_frame_ = current_frame_;
    mapper_.AddKeyFrame(ref_frame_);
    //MakeKeyFrame - new map points creation
  }

  current_frame_->SetReferenceFrame(ref_frame_);

  } //lock block

  return true;
}

void Tracker::EstimateVelocity() {
  velocity_ = current_frame()->GetFramePose() * last_frame_->Twc();
}

} //namespace orbslam_wr

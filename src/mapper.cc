#include "mapper.h"

#include <chrono>
#include <iostream>
#include <limits>

namespace orbslam_wr {

Mapper::Mapper() : keep_running_(false) {

}

Mapper::~Mapper() {
  map_.Reset();
  keyframes_.clear();
  std::queue<std::shared_ptr<Frame>>().swap(incoming_keyframes_queue_);
  std::queue<std::shared_ptr<Frame>>().swap(keyframes_queue_);
}

void Mapper::Start() {
  keep_running_ = true;
  thread_ = std::thread(&Mapper::Run, this);
}

void Mapper::Stop() {
  keep_running_ = false;
  if (thread_.joinable())
    thread_.join();
}

void Mapper::Run() {
  std::chrono::seconds duration(1);

  int ba_count = 0;

  for (;;) {
    if (!keep_running_)
      break;

    ReloadKeyFrames();

    if(keyframes_queue_.empty()) {
      std::this_thread::sleep_for(duration);
      continue;
    }

    do {
      std::shared_ptr<Frame> frame = std::move(keyframes_queue_.front());
      keyframes_queue_.pop();

      //Find near frames
      std::vector<std::pair<double, int>> best_dists_and_indices;
      double best_dist = std::numeric_limits<double>::max();
      float best_angle = 0;
      int best_idx = -1;
      for (int i = 0; i < keyframes_.size(); i++) {
        cv::Mat n = (cv::Mat_<float>(3,1) << 0, 0, 1);
        cv::Mat n_ref = keyframes_[i]->Rcw()*n;
        cv::Mat n_new = frame->Rcw()*n;
        float angle_diff = n_ref.dot(n_new);
        cv::Mat t_diff = keyframes_[i]->twc() - frame->twc();
        double dist = sqrt(t_diff.at<float>(0)*t_diff.at<float>(0) +
                           t_diff.at<float>(1)*t_diff.at<float>(1) +
                           t_diff.at<float>(2)*t_diff.at<float>(2));

        if (dist < best_dist && angle_diff > best_angle) {
          if (angle_diff < 0.5) {//cos 60 degree
            continue;
          }

          best_dist = dist;
          best_idx = i;
          best_dists_and_indices.push_back(std::make_pair(dist, i));

          if (best_dists_and_indices.size() > 3) {
            best_dists_and_indices.erase(best_dists_and_indices.begin());
          }
        }
      }


      for (int i = 0; i < best_dists_and_indices.size(); i++){
        best_dist = best_dists_and_indices[i].first;
        best_idx = best_dists_and_indices[i].second;

        std::cout << "best dist: " << best_dist << " " << best_idx <<std::endl;

        //Match with best keyframe and insert additional map points;
        FeatureMatcher::WindowSize window_size(40, 40);
        //int matched = feature_matcher_.RefToFrameORBMatch(
        //  keyframes_[best_idx].get(), frame.get(), window_size);

        int matched = feature_matcher_.KeyframeToKeyframeORBMatch(
          keyframes_[best_idx].get(), frame.get());

        //Map Point culling test

        std::vector<std::shared_ptr<MapPoint>>& map_points = map_.map_points();

        for (std::vector<std::shared_ptr<MapPoint>>::iterator it
               = map_points.begin(); it != map_points.end(); ) {
          if ((*it)->observer().size() < 2 && (*it)->tracking_score < 3) {
            (*it)->is_mapped = false;
            it = map_points.erase(it);
          }
          else {
            ++it;
          }
        }

        std::cout << "key points matches: " << matched << std::endl;
        /*if (matched > 20) {
          int count = graph_optimizer_.PoseOnlyOptimize(frame.get());
           std::cout << "optimize!: " << count << std::endl;
        }*/

        //ToDo:: near frame network making || reference frame switching
        //       Match New KF with candidates

      }

      {
      std::lock_guard<std::mutex> lock(map_data_lock_);
      AddMapPointsInFrame(frame);
      //frame->TransformDescToBow();
      }// lock

      keyframes_.push_back(frame);

    } while(!keyframes_queue_.empty());

    std::cout << "Mapper::Run() - keyframe size" << keyframes_.size() << std::endl;

    if (!keep_running_)
      break;

    //Test BA
    ++ba_count;

    std::vector<std::shared_ptr<MapPoint>>& map_points = map_.map_points();

    if (ba_count % 10 == 0 && map_points.size() > 0) {
      std::vector<cv::Mat> optimized_poses;
      std::vector<cv::Mat> optimized_map_points;
      std::cout << "Mapper::Run() - BA start" << std::endl;
      graph_optimizer_.BundleAdjustment(keyframes_, map_,
                                        optimized_poses, optimized_map_points);
      std::cout << "Mapper::Run() - BA done" << std::endl;

      //Update keyframe poses and map points
      //lock should be held here (not during B.A)
      //lock keyframes and map points (can be used concurrently in tracking)
      std::lock_guard<std::mutex> lock(map_data_lock_);
      for (int i = 0; i < optimized_poses.size(); i++) {
        keyframes_[i]->SetPose(optimized_poses[i]);
        keyframes_[i]->UpdateFeaturesWorldPose();
      }

      std::vector<std::shared_ptr<MapPoint>> map_points = map_.map_points();

      for (int i = 0; i < optimized_map_points.size(); i++) {
        map_points[i]->pos_world = optimized_map_points[i];
      }
    }

  }
}

void Mapper::AddKeyFrame(std::shared_ptr<Frame>& frame) {
  //FIX ME: This is because initial KeyFrame is the first reference.
  //Need to be replaced by generic algorithm.
  if (keyframes_.size() == 0) {
    AddMapPointsInFrame(frame);
    keyframes_.push_back(frame);
  }
  else {
    std::lock_guard<std::mutex> lock(incoming_kf_queue_lock_);
    incoming_keyframes_queue_.push(frame);
  }
}

void Mapper::Reset() {
  Stop();
  map_.Reset();
  keyframes_.clear();
  std::queue<std::shared_ptr<Frame>>().swap(incoming_keyframes_queue_);
  std::queue<std::shared_ptr<Frame>>().swap(keyframes_queue_);
  Start();
}

void Mapper::AddMapPointsInFrame(std::shared_ptr<Frame>& frame) {
  std::vector<std::shared_ptr<FeaturePoint3D>>& feature_points
    = frame->feature_points();
  int feature_points_size = frame->feature_points_size();

  std::vector<std::shared_ptr<MapPoint>>& map_points
    = frame->map_points();

  frame->map_point_match_size = 0;

  for (int i = 0; i < feature_points_size; i++) {
    if (map_points[i]) {
      map_points[i]->AddObserver(frame.get());
      map_.AddMapPoint(map_points[i]);
      continue;
    }

    cv::Mat pos = frame->FeatureToWorldPose(i);
    if (pos.empty()) {
      continue;
    }

    map_points[i] = std::make_shared<MapPoint>();
    map_points[i]->pos_world = pos.clone();
    map_points[i]->desc = feature_points[i]->desc;
    map_points[i]->normal = pos - frame->twc();
    map_points[i]->normal /= cv::norm(map_points[i]->normal);
    map_points[i]->AddObserver(frame.get());

    map_.AddMapPoint(map_points[i]);
  }
}

void Mapper::ReloadKeyFrames() {
  std::lock_guard<std::mutex> lock(incoming_kf_queue_lock_);

  assert(keyframes_queue_.empty());

  incoming_keyframes_queue_.swap(keyframes_queue_);
}

} //namespace orbslam_wr

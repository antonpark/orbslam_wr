#include "graph_optimizer.h"

#include "3rdParty/g2o/g2o/core/block_solver.h"
#include "3rdParty/g2o/g2o/core/optimization_algorithm_levenberg.h"
#include "3rdParty/g2o/g2o/solvers/linear_solver_eigen.h"
#include "3rdParty/g2o/g2o/types/types_six_dof_expmap.h"
#include "3rdParty/g2o/g2o/core/robust_kernel_impl.h"
#include "3rdParty/g2o/g2o/solvers/linear_solver_dense.h"


namespace orbslam_wr {

const float kChiSquare3DoF_95 = 7.815f;
const float kChiSquare2DoF_95 = 5.991f;

const float kSqrtChiSquare3DoF_95 = sqrt(kChiSquare3DoF_95);
const float kSqrtChiSquare2DoF_95 = sqrt(kChiSquare2DoF_95);

Eigen::Matrix<double, 3, 1> CVMatToVector3d(const cv::Mat& mat3)
{
    Eigen::Matrix<double, 3, 1> vector3d;
    vector3d << mat3.at<float>(0), mat3.at<float>(1), mat3.at<float>(2);

    return vector3d;
}


cv::Mat Vector3dToCVMat(const Eigen::Matrix<double,3,1> &vector3d)
{
    cv::Mat cv_mat(3, 1, CV_32F);
    for(int i = 0; i < 3; i++)
      cv_mat.at<float>(i)=vector3d(i);

    return cv_mat.clone();
}


g2o::SE3Quat CVMatToSE3Quat(cv::Mat& T) {
  Eigen::Matrix<double, 3, 3> R;
  R << T.at<float>(0, 0), T.at<float>(0, 1), T.at<float>(0, 2),
       T.at<float>(1, 0), T.at<float>(1, 1), T.at<float>(1, 2),
       T.at<float>(2, 0), T.at<float>(2, 1), T.at<float>(2, 2);

  Eigen::Matrix<double, 3, 1> t;
  t << T.at<float>(0, 3) ,T.at<float>(1, 3) ,T.at<float>(2, 3);

  return g2o::SE3Quat(R, t);
}

cv::Mat SE3QuatToCVMat(g2o::SE3Quat& se3) {
  Eigen::Matrix<double, 4, 4> eigen_mat = se3.to_homogeneous_matrix();
  cv::Mat cv_mat(4, 4, CV_32F);

  for(int i = 0; i < 4; i++) {
    for(int j = 0; j < 4; j++) {
      cv_mat.at<float>(i, j) = eigen_mat(i, j);
    }
  }

  return cv_mat.clone();
}

GraphOptimizer::GraphOptimizer() {


}

int GraphOptimizer::PoseOnlyOptimize(Frame* frame) {

  //feature points setup
  int feature_point_size = frame->feature_points_size();

  std::vector<std::shared_ptr<FeaturePoint3D>>& feature_points
    = frame->feature_points();

  std::vector<g2o::EdgeStereoSE3ProjectXYZOnlyPose*> edges_feature_points;
  std::vector<int> edged_feature_indices;

  std::vector<g2o::EdgeSE3ProjectXYZOnlyPose*> mono_edges_feature_points;
  std::vector<int> mono_edged_feature_indices;

  //g2o initialization
  g2o::SparseOptimizer pose_only_optimizer;

  g2o::BlockSolver_6_3::LinearSolverType* linear_solver;
  linear_solver
    = new g2o::LinearSolverDense<g2o::BlockSolver_6_3::PoseMatrixType>();

  g2o::BlockSolver_6_3* block_solver = new g2o::BlockSolver_6_3(linear_solver);

  g2o::OptimizationAlgorithmLevenberg* algorithm
    = new g2o::OptimizationAlgorithmLevenberg(block_solver);

  pose_only_optimizer.setAlgorithm(algorithm);

  g2o::VertexSE3Expmap* vertex_frame_pose = new g2o::VertexSE3Expmap();

  vertex_frame_pose->setEstimate(CVMatToSE3Quat(frame->GetFramePose()));
  vertex_frame_pose->setId(0);
  vertex_frame_pose->setFixed(false);

  pose_only_optimizer.addVertex(vertex_frame_pose);

  int count = 0;

  std::vector<float> inv_scale_sqrs
    = frame->GetFeatureExtractor()->GetInverseScaleSigmaSquares();

  for (int i = 0; i < feature_point_size; i++) {
    if(feature_points[i]->pos_world.empty()) {
      continue;
    }

    count++;

    if (feature_points[i]->disparity_x > 0) {
      Eigen::Matrix<double, 3, 1> observation;
      observation << feature_points[i]->undist_keypoint.pt.x,
                     feature_points[i]->undist_keypoint.pt.y,
                     feature_points[i]->disparity_x;

      g2o::EdgeStereoSE3ProjectXYZOnlyPose* edge
        = new g2o::EdgeStereoSE3ProjectXYZOnlyPose();

      edge->setVertex(0,
        dynamic_cast<g2o::OptimizableGraph::Vertex*>(
          pose_only_optimizer.vertex(0)));
      edge->setMeasurement(observation);

      //multiply inverse sigma of feature octave to info matrix
      Eigen::Matrix3d info = Eigen::Matrix3d::Identity()
                              * inv_scale_sqrs[feature_points[i]->keypoint.octave];
      edge->setInformation(info);

      g2o::RobustKernelHuber* huber_kernel = new g2o::RobustKernelHuber();

      edge->setRobustKernel(huber_kernel);

      huber_kernel->setDelta(kSqrtChiSquare3DoF_95);

      edge->fx = Frame::fx();
      edge->fy = Frame::fy();
      edge->cx = Frame::cx();
      edge->cy = Frame::cy();
      edge->bf = Frame::bf();

      cv::Mat& feature_point_pose_world = feature_points[i]->pos_world;

      edge->Xw[0] = feature_point_pose_world.at<float>(0);
      edge->Xw[1] = feature_point_pose_world.at<float>(1);
      edge->Xw[2] = feature_point_pose_world.at<float>(2);

      pose_only_optimizer.addEdge(edge);
      edged_feature_indices.push_back(i);
      edges_feature_points.push_back(edge);
    }
    else {
      Eigen::Matrix<double, 2, 1> observation;
      observation << feature_points[i]->undist_keypoint.pt.x,
                     feature_points[i]->undist_keypoint.pt.y;

      g2o::EdgeSE3ProjectXYZOnlyPose* edge
        = new g2o::EdgeSE3ProjectXYZOnlyPose();

      edge->setVertex(0,
        dynamic_cast<g2o::OptimizableGraph::Vertex*>(
          pose_only_optimizer.vertex(0)));
      edge->setMeasurement(observation);

      //multiply inverse sigma of feature octave to info matrix
      Eigen::Matrix2d info = Eigen::Matrix2d::Identity()
                              * inv_scale_sqrs[feature_points[i]->keypoint.octave];
      edge->setInformation(info);

      g2o::RobustKernelHuber* huber_kernel = new g2o::RobustKernelHuber();

      edge->setRobustKernel(huber_kernel);

      huber_kernel->setDelta(kSqrtChiSquare2DoF_95);

      edge->fx = Frame::fx();
      edge->fy = Frame::fy();
      edge->cx = Frame::cx();
      edge->cy = Frame::cy();

      cv::Mat& feature_point_pose_world = feature_points[i]->pos_world;

      edge->Xw[0] = feature_point_pose_world.at<float>(0);
      edge->Xw[1] = feature_point_pose_world.at<float>(1);
      edge->Xw[2] = feature_point_pose_world.at<float>(2);

      pose_only_optimizer.addEdge(edge);
      mono_edged_feature_indices.push_back(i);
      mono_edges_feature_points.push_back(edge);
    }
  }

  if (count == 0) {
    return 0;
  }

  pose_only_optimizer.initializeOptimization(0);
  pose_only_optimizer.optimize(10);

  //iterative outlier removal
  //this block can be looped
  {
  for (int i = 0; i < edged_feature_indices.size(); i++) {
    g2o::EdgeStereoSE3ProjectXYZOnlyPose* edge = edges_feature_points[i];
    edge->computeError();

    if(edge->chi2() > kChiSquare3DoF_95) {
      edge->setLevel(1);
      feature_points[edged_feature_indices[i]]->pos_world = cv::Mat();
      count--;
    }
  }

  for (int i = 0; i < mono_edged_feature_indices.size(); i++) {
    g2o::EdgeSE3ProjectXYZOnlyPose* edge = mono_edges_feature_points[i];
    edge->computeError();

    if(edge->chi2() > kChiSquare2DoF_95) {
      edge->setLevel(1);
      feature_points[mono_edged_feature_indices[i]]->pos_world = cv::Mat();
      count--;
    }
  }

  if (count == 0) {
    return 0;
  }

  vertex_frame_pose->setEstimate(CVMatToSE3Quat(frame->GetFramePose()));
  pose_only_optimizer.initializeOptimization(0);
  pose_only_optimizer.optimize(10);
  }

  g2o::VertexSE3Expmap* pose_recovered
    = static_cast<g2o::VertexSE3Expmap*>(pose_only_optimizer.vertex(0));

  g2o::SE3Quat estimation = pose_recovered->estimate();

  cv::Mat optimized_pose = SE3QuatToCVMat(estimation);
  frame->SetPose(optimized_pose);

  return count;
}

void GraphOptimizer::BundleAdjustment(
    std::vector<std::shared_ptr<Frame>>& keyframes,
    Map& map,
    std::vector<cv::Mat>& out_poses,
    std::vector<cv::Mat>& out_map_points) {
  //g2o initialization
  g2o::SparseOptimizer ba_optimizer;

  g2o::BlockSolver_6_3::LinearSolverType* linear_solver;
  linear_solver
    = new g2o::LinearSolverEigen<g2o::BlockSolver_6_3::PoseMatrixType>();

  g2o::BlockSolver_6_3* block_solver = new g2o::BlockSolver_6_3(linear_solver);

  g2o::OptimizationAlgorithmLevenberg* algorithm
    = new g2o::OptimizationAlgorithmLevenberg(block_solver);

  ba_optimizer.setAlgorithm(algorithm);

  //
  int keyframes_size = keyframes.size();

  std::vector<float> inv_scale_sqrs
    = keyframes[0]->GetFeatureExtractor()->GetInverseScaleSigmaSquares();

  int current_frame_global_number = Frame::global_frame_number;

  //for map points: set map point vertex
  std::vector<std::shared_ptr<MapPoint>>& map_points = map.map_points();
  int map_points_size = map_points.size();

  std::cout << "map points size: " << map_points_size << std::endl;

  for (int i = 0; i < map_points_size; i++) {
      g2o::VertexSBAPointXYZ* vertex_map_point_pose
        = new g2o::VertexSBAPointXYZ();
      vertex_map_point_pose->setEstimate(
        CVMatToVector3d(map_points[i]->pos_world));
      vertex_map_point_pose->setId(
        current_frame_global_number + 1 + map_points[i]->id);
      vertex_map_point_pose->setMarginalized(true);
      vertex_map_point_pose->setFixed(false);

      ba_optimizer.addVertex(vertex_map_point_pose);
  }

  //for keyframes: set key frame vertex
  for (int i = 0; i < keyframes_size; i++) {
    g2o::VertexSE3Expmap * vertex_frame_pose = new g2o::VertexSE3Expmap();

    vertex_frame_pose->setEstimate(CVMatToSE3Quat(keyframes[i]->GetFramePose()));
    vertex_frame_pose->setId(keyframes[i]->id);
    vertex_frame_pose->setFixed((i == 0) ? true:false);

    ba_optimizer.addVertex(vertex_frame_pose);
  }

  for (int i = 0; i < keyframes_size; i++) {
    //for map points in keyframes: set map point vertex, set edge between kf v & mp v
    std::vector<std::shared_ptr<MapPoint>>& frame_map_points
      = keyframes[i]->map_points();

    std::vector<std::shared_ptr<FeaturePoint3D>>& feature_points
      = keyframes[i]->feature_points();
    int feature_points_size = feature_points.size();

    int edge_count = 0;

    for (int j = 0; j < feature_points_size; j++) {
      if(!frame_map_points[i] || !frame_map_points[i]->is_mapped) {
        continue;
      }

      edge_count++;

      if (feature_points[i]->disparity_x > 0) {
        Eigen::Matrix<double, 3, 1> observation;
        observation << feature_points[i]->undist_keypoint.pt.x,
                       feature_points[i]->undist_keypoint.pt.y,
                       feature_points[i]->disparity_x;

        g2o::EdgeStereoSE3ProjectXYZ* edge = new g2o::EdgeStereoSE3ProjectXYZ();

        edge->setVertex(1,
          dynamic_cast<g2o::OptimizableGraph::Vertex*>(
            ba_optimizer.vertex(keyframes[i]->id)));
        edge->setVertex(0,
          dynamic_cast<g2o::OptimizableGraph::Vertex*>(
            ba_optimizer.vertex(
              current_frame_global_number + 1 + frame_map_points[i]->id)));
        edge->setMeasurement(observation);

        //multiply inverse sigma of feature octave to info matrix
        Eigen::Matrix3d info = Eigen::Matrix3d::Identity()
                                * inv_scale_sqrs[feature_points[i]->keypoint.octave];
        edge->setInformation(info);

        g2o::RobustKernelHuber* huber_kernel = new g2o::RobustKernelHuber();

        edge->setRobustKernel(huber_kernel);

        huber_kernel->setDelta(kSqrtChiSquare3DoF_95);

        edge->fx = Frame::fx();
        edge->fy = Frame::fy();
        edge->cx = Frame::cx();
        edge->cy = Frame::cy();
        edge->bf = Frame::bf();

        ba_optimizer.addEdge(edge);
      }
      else {
        Eigen::Matrix<double, 2, 1> observation;
        observation << feature_points[i]->undist_keypoint.pt.x,
                       feature_points[i]->undist_keypoint.pt.y;

        g2o::EdgeSE3ProjectXYZ* edge = new g2o::EdgeSE3ProjectXYZ();

        edge->setVertex(1,
          dynamic_cast<g2o::OptimizableGraph::Vertex*>(
            ba_optimizer.vertex(keyframes[i]->id)));
        edge->setVertex(0,
          dynamic_cast<g2o::OptimizableGraph::Vertex*>(
            ba_optimizer.vertex(
              current_frame_global_number + 1 + frame_map_points[i]->id)));
        edge->setMeasurement(observation);

        //multiply inverse sigma of feature octave to info matrix
        Eigen::Matrix2d info = Eigen::Matrix2d::Identity()
                                * inv_scale_sqrs[feature_points[i]->keypoint.octave];
        edge->setInformation(info);

        g2o::RobustKernelHuber* huber_kernel = new g2o::RobustKernelHuber();

        edge->setRobustKernel(huber_kernel);

        huber_kernel->setDelta(kSqrtChiSquare2DoF_95);

        edge->fx = Frame::fx();
        edge->fy = Frame::fy();
        edge->cx = Frame::cx();
        edge->cy = Frame::cy();

        ba_optimizer.addEdge(edge);
      }
    }
  }

  //optimize
  std::cout << "optimize start!!" << std::endl;
  ba_optimizer.initializeOptimization();
  ba_optimizer.optimize(5);
  std::cout << "optimize end!!" << std::endl;

  //apply optimzed results to kfs and mps
  out_poses.resize(keyframes_size);
  out_map_points.resize(map_points_size);

  for (int i = 0; i < keyframes_size; i++) {
    g2o::VertexSE3Expmap* pose_recovered
      = static_cast<g2o::VertexSE3Expmap*>(
        ba_optimizer.vertex(keyframes[i]->id));

    g2o::SE3Quat estimation = pose_recovered->estimate();

    cv::Mat optimized_pose = SE3QuatToCVMat(estimation);
    //keyframes[i]->SetPose(optimized_pose);
    out_poses[i] = optimized_pose;
  }

  for (int i = 0; i < map_points_size; i++) {
    g2o::VertexSBAPointXYZ* pose_recovered
      = static_cast<g2o::VertexSBAPointXYZ*>(
        ba_optimizer.vertex(
          current_frame_global_number + 1 + map_points[i]->id));
    //map_points[i]->pos_world = Vector3dToCVMat(pose_recovered->estimate());
    out_map_points[i] = Vector3dToCVMat(pose_recovered->estimate());
  }
}

} //namespace orbslam_wr

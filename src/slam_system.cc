#include "slam_system.h"

#include <opencv2/imgproc/imgproc.hpp>

#include "frame.h"

namespace orbslam_wr {

SLAMSystem::SLAMSystem() : feature_extractor_(1000, 1.2, 8, 20, 7),
                           tracker_initialized(false) {

}

void SLAMSystem::LoadSettingsFile(std::string file_path) {
  yaml_settings_.open(file_path, cv::FileStorage::READ);
  if(!yaml_settings_.isOpened())
  {
     std::cerr << "Failed to open settings file at: "
               << file_path << std::endl;
     exit(-1);
  }
}

void SLAMSystem::LoadORBVocabularyFile(std::string file_path) {
  std::cout << "Loading ORB vocabulary file.." << std::endl;
  if(!orb_vocabulary_.loadFromTextFile(file_path))
  {
     std::cerr << "Failed to open orb vocabulary file at: "
               << file_path << std::endl;
     exit(-1);
  }

  std::cout << "Complete!" << std::endl;
}

void SLAMSystem::InitializeSystem() {
  if(!yaml_settings_.isOpened())
  {
     std::cerr << "Please load proper settings file first." << std::endl;
     exit(-1);
  }

  depth_map_factor_ = yaml_settings_["DepthMapFactor"];
  depth_map_factor_inv_ = 1.0f/depth_map_factor_;

  orbslam_wr::Frame::CameraParam cam_param;
  cam_param.fx = yaml_settings_["Camera.fx"];
  cam_param.fy = yaml_settings_["Camera.fy"];
  cam_param.cx = yaml_settings_["Camera.cx"];
  cam_param.cy = yaml_settings_["Camera.cy"];
  cam_param.bf = yaml_settings_["Camera.bf"];
  cam_param.dist_coef = cv::Mat(4, 1, CV_32F);
  cam_param.dist_coef.at<float>(0) = yaml_settings_["Camera.k1"];
  cam_param.dist_coef.at<float>(1) = yaml_settings_["Camera.k2"];
  cam_param.dist_coef.at<float>(2) = yaml_settings_["Camera.p1"];
  cam_param.dist_coef.at<float>(3) = yaml_settings_["Camera.p2"];
  cam_param.image_bound_x = yaml_settings_["Camera.width"];
  cam_param.image_bound_y = yaml_settings_["Camera.height"];

  orbslam_wr::Frame::SetCameraParam(cam_param);
}

void SLAMSystem::SendRGBDImage(cv::Mat& rgb_img, cv::Mat& depth_img) {
  //convert to mono if not
  if (rgb_img.channels() == 3) {
    cv::cvtColor(rgb_img, rgb_img, CV_RGB2GRAY);
  }
  else if (rgb_img.channels() == 4) {
    cv::cvtColor(rgb_img, rgb_img, CV_RGBA2GRAY);
  }

  //normalize depth data
  if((fabs(depth_map_factor_inv_- 1.0f)>1e-5) || depth_img.type()!=CV_32F) {
        depth_img.convertTo(depth_img, CV_32F, depth_map_factor_inv_);
  }

  if (tracker_initialized) {
    if(!tracker_.Track(new Frame(rgb_img, depth_img, &feature_extractor_, &orb_vocabulary_))) {
      //ToDo::Try relocalization
      //if failed, reset or continue tracking with disconnected key frames-later find connection
      //TBD:
      tracker_initialized = false;
    }
  }
  else {
    if(tracker_.Init(new Frame(rgb_img, depth_img, &feature_extractor_, &orb_vocabulary_))) {
      tracker_initialized = true;
    }
  }
}

} //namespace orbslam_wr

#include "frame.h"
#include "orb_vocabulary.h"

namespace orbslam_wr {

float Frame::fx_ = 0.0f;
float Frame::fy_ = 0.0f;
float Frame::cx_ = 0.0f;
float Frame::cy_ = 0.0f;
float Frame::fx_inv_ = 0.0f;
float Frame::fy_inv_ = 0.0f;
float Frame::bf_ = 0.0f;
cv::Mat Frame::dist_coef_ = cv::Mat();
cv::Mat Frame::K_ = cv::Mat();
float Frame::img_bound_x_max_ = 640.0f;
float Frame::img_bound_x_min_ = 0.0f;
float Frame::img_bound_y_max_ = 320.0f;
float Frame::img_bound_y_min_ = 0.0f;
uint64_t Frame::global_frame_number = 0;

Frame::Frame(cv::Mat& rgb_img, cv::Mat& depth_img,
             FeatureExtractor* feature_extractor,
             ORBVocabulary* orb_vocabulary)
    : feature_extractor_(feature_extractor),
      orb_vocabulary_(orb_vocabulary),
      map_point_match_size(0), Tcw_(cv::Mat::eye(4,4,CV_32F)) {

  SetPose(Tcw_);
  feature_extractor_->ExtractFeaturePoints3D(
    rgb_img, cv::Mat(), depth_img, feature_points_);

  feature_points_size_ = feature_points_.size();
  map_points_.resize(feature_points_size_);
  id = global_frame_number++;
}

cv::Mat Frame::FeatureToWorldPose(int idx) {
  if (feature_points_size_ < idx) {
    return cv::Mat();
  }

  std::shared_ptr<FeaturePoint3D> point = feature_points_[idx];

  float z = point->depth;

  if (z <= 0) {
    return cv::Mat();
  }

  float u = point->undist_keypoint.pt.x;
  float v = point->undist_keypoint.pt.y;
  float x = (u-cx_)*z*fx_inv_;
  float y = (v-cy_)*z*fy_inv_;
  cv::Mat Pc = (cv::Mat_<float>(3,1) << x, y, z);

  cv::Mat result_point = Rwc_*Pc - Rwc_*tcw_;

  return result_point;
}

Frame::~Frame() {

}

void Frame::UpdateFeaturesWorldPose() {
  for (int i = 0; i < feature_points_size_; i++) {
    if (!feature_points_[i]->pos_world.empty())
      continue;

    cv::Mat pos = FeatureToWorldPose(i);

    if (pos.empty())
      continue;

    feature_points_[i]->pos_world = pos.clone();
  }
}

void Frame::UpdatePose() {
  if (!ref_frame_ || Tcr_.empty())
    return;

  cv::Mat Tcw_updated = Tcr_ * ref_frame_->Tcw();
  SetPose(Tcw_updated);
}

void Frame::SetPose(cv::Mat& pose) {
  Tcw_ = pose.clone();
  Rcw_ = Tcw_.rowRange(0,3).colRange(0,3);
  Rwc_ = Rcw_.t();
  tcw_ = Tcw_.rowRange(0,3).col(3);
  twc_ = -Rwc_*tcw_;
  Twc_ = cv::Mat::eye(4, 4, Tcw_.type());
  Rwc_.copyTo(Twc_.rowRange(0, 3).colRange(0, 3));
  twc_.copyTo(Twc_.rowRange(0, 3).col(3));
}

void Frame::TransformDescToBow() {
  std::vector<cv::Mat> descriptors;
  descriptors.reserve(feature_points_size_);

  for (int i = 0; i < feature_points_size_; i++) {
    descriptors.push_back(feature_points_[i]->desc);
  }

  orb_vocabulary_->transform(descriptors, BoW_vector_, BoW_Feature_vector_, 4);
}

//static
void Frame::SetCameraParam(CameraParam& param) {
  fx_ = param.fx;
  fy_ = param.fy;
  cx_ = param.cx;
  cy_ = param.cy;
  fx_inv_ = 1.0f/fx_;
  fy_inv_ = 1.0f/fy_;
  bf_ = param.bf;
  dist_coef_ = param.dist_coef.clone();
  K_ = cv::Mat::eye(3, 3, CV_32F);
  K_.at<float>(0, 0) = fx_;
  K_.at<float>(1, 1) = fy_;
  K_.at<float>(0, 2) = cx_;
  K_.at<float>(1, 2) = cy_;

  if (dist_coef_.at<float>(0) != 0.0f) {
    cv::Mat mat(4, 2, CV_32F);
    mat.at<float>(0,0)= 0.0;
    mat.at<float>(0,1)= 0.0;
    mat.at<float>(1,0)= param.image_bound_x;
    mat.at<float>(1,1)= 0.0;
    mat.at<float>(2,0)= 0.0;
    mat.at<float>(2,1)= param.image_bound_y;
    mat.at<float>(3,0)= param.image_bound_x;
    mat.at<float>(3,1)= param.image_bound_y;

    // Undistort corners
    mat = mat.reshape(2);
    cv::undistortPoints(mat, mat, K_, dist_coef_, cv::Mat(), K_);
    mat = mat.reshape(1);

    img_bound_x_min_ = std::min(mat.at<float>(0,0), mat.at<float>(2,0));
    img_bound_x_max_ = std::max(mat.at<float>(1,0), mat.at<float>(3,0));
    img_bound_y_min_ = std::min(mat.at<float>(0,1), mat.at<float>(1,1));
    img_bound_y_max_ = std::max(mat.at<float>(2,1), mat.at<float>(3,1));
  }
  else {
    img_bound_x_min_ = 0.0f;
    img_bound_x_max_ = param.image_bound_x;
    img_bound_y_min_ = 0.0f;
    img_bound_y_max_ = param.image_bound_y;
  }
}

void Frame::SetReferenceFrame(std::shared_ptr<Frame>& frame) {
  ref_frame_ = frame;

  Tcr_ = Tcw_ * ref_frame_->Twc();
}

bool Frame::IsPointInImageBound(cv::Mat point) {
  float xc = point.at<float>(0);
  float yc = point.at<float>(1);
  float invzc = 1.0/point.at<float>(2);

  float u = fx_*xc*invzc + cx_;
  float v = fy_*yc*invzc + cy_;

  if(u < img_bound_x_min_ || u > img_bound_x_max_)
      return false;
  if(v < img_bound_y_min_ || v > img_bound_y_max_)
      return false;

  return true;
}

} //namespace orbslam_wr

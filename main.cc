
#include <opencv2/core/core.hpp>
#include "opencv2/highgui/highgui.hpp"

int main() {
  cv::Mat img(320, 240, CV_16UC3, cv::Scalar(0, 50000, 50000));

  std::vector<int> compression_params;

  compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
  compression_params.push_back(98);

  cv::imwrite("test.jpg", img, compression_params);

  cv::namedWindow("testWindow", CV_WINDOW_AUTOSIZE);

  cv::imshow("testWindow", img);

  cv::waitKey(0);

  cv::destroyWindow("testWindow");

  return 0;
}

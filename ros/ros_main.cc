#include <chrono>

#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud.h>
#include <tf/transform_broadcaster.h>

#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <cv_bridge/cv_bridge.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <Eigen/Dense>

#include "slam_system.h"

ros::Publisher pub;
orbslam_wr::SLAMSystem slam_system;
tf::TransformBroadcaster* odom_broadcaster;

void Callback(const sensor_msgs::ImageConstPtr& msg_img,
              const sensor_msgs::ImageConstPtr& msg_depth) {
  //std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();

  //Grab image message and convert to cv world//////////////////////////////////
  cv_bridge::CvImageConstPtr cv_img;

  try {
    cv_img = cv_bridge::toCvShare(msg_img);
  }
  catch (cv_bridge::Exception& e) {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    printf("cv_bridge exception1\n");
    return;
  }

  cv_bridge::CvImageConstPtr cv_depth;

  try {
    cv_depth = cv_bridge::toCvShare(msg_depth);
  }
  catch (cv_bridge::Exception& e) {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    printf("cv_bridge exception2\n");
    return;
  }

  /////////////////////////////////////////////////////////
  cv::Mat mat = cv_img->image;

  //convert to mono if not
  if (mat.channels() == 3) {
    cv::cvtColor(mat, mat, CV_RGB2GRAY);
  }
  else if (mat.channels() == 4) {
    cv::cvtColor(mat, mat, CV_RGBA2GRAY);
  }

  cv::Mat mat_depth = cv_depth->image;

  /////slam system
  slam_system.SendRGBDImage(mat, mat_depth);

  orbslam_wr::Frame* frame = slam_system.GetCurrentFrame();
  orbslam_wr::Mapper& mapper = slam_system.GetMapper();

  //ROS msg publish/////////////////////////////////////////////////////////////
  //Map points publish
  sensor_msgs::PointCloud msg_mappoint;
  msg_mappoint.header.frame_id = "camera_ref";

  std::vector<std::shared_ptr<orbslam_wr::MapPoint>>& map_points
    = mapper.map().map_points();

  {
  std::unique_lock<std::mutex> lock(mapper.map_data_lock());
  for (int i = 0; i < map_points.size(); i++) {
    cv::Mat point = map_points[i]->pos_world;

    if (point.empty()) {
      continue;
    }

    geometry_msgs::Point32 point32;
    point32.x = point.at<float>(0);
    point32.y = point.at<float>(1);
    point32.z = point.at<float>(2);
    msg_mappoint.points.push_back(point32);
  }
  }

  pub.publish(msg_mappoint);

  std::vector<std::shared_ptr<orbslam_wr::FeaturePoint3D>>& feature_points
    = frame->feature_points();

  /*
  //3d feature points publish
  sensor_msgs::PointCloud msg;
  msg.header.frame_id = "camera_ref";

  for (int i = 0; i < feature_points.size(); i++) {
    cv::Mat point = feature_points[i]->pos_world;

    if (point.empty()) {
      continue;
    }

    geometry_msgs::Point32 point32;
    point32.x = point.at<float>(0);
    point32.y = point.at<float>(1);
    point32.z = point.at<float>(2);
    msg.points.push_back(point32);
  }

  pub.publish(msg);
  */

  //Camera transform broadcast
  cv::Mat twc = frame->twc();
  cv::Mat Rwc = frame->Rwc();

  Eigen::Matrix3f mat_eigen;
  mat_eigen << Rwc.at<float>(0,0), Rwc.at<float>(0,1), Rwc.at<float>(0,2),
               Rwc.at<float>(1,0), Rwc.at<float>(1,1), Rwc.at<float>(1,2),
               Rwc.at<float>(2,0), Rwc.at<float>(2,1), Rwc.at<float>(2,2);

  Eigen::Quaternionf q(mat_eigen);

  //first, we'll publish the transform over tf
  geometry_msgs::TransformStamped odom_trans;
  odom_trans.header.frame_id = "camera_ref";
  odom_trans.child_frame_id = "camera_frame";
  odom_trans.transform.translation.x = twc.at<float>(0);
  odom_trans.transform.translation.y = twc.at<float>(1);
  odom_trans.transform.translation.z = twc.at<float>(2);
  geometry_msgs::Quaternion odom_quat;
  odom_quat.w = q.w();
  odom_quat.x = q.x();
  odom_quat.y = q.y();
  odom_quat.z = q.z();

  odom_trans.transform.rotation = odom_quat;

  odom_broadcaster->sendTransform(odom_trans);

  /*
  //Camera diff reference transform broadcast
  cv::Mat Tcr = frame->Tcr();
  if (!Tcr.empty()) {
    Tcr = frame->ref_frame()->Tcw() * Tcr;
    cv::Mat tcr = Tcr.rowRange(0,3).col(3);

    Eigen::Matrix3f mat_eigen2;
    mat_eigen2 << Tcr.at<float>(0,0), Tcr.at<float>(0,1), Tcr.at<float>(0,2),
                 Tcr.at<float>(1,0), Tcr.at<float>(1,1), Tcr.at<float>(1,2),
                 Tcr.at<float>(2,0), Tcr.at<float>(2,1), Tcr.at<float>(2,2);

    Eigen::Quaternionf q2(mat_eigen2);

    //first, we'll publish the transform over tf
    geometry_msgs::TransformStamped odom_trans2;
    odom_trans2.header.frame_id = "camera_ref";
    odom_trans2.child_frame_id = "camera_frame_ref_diff";
    odom_trans2.transform.translation.x = tcr.at<float>(0);
    odom_trans2.transform.translation.y = tcr.at<float>(1);
    odom_trans2.transform.translation.z = tcr.at<float>(2);
    geometry_msgs::Quaternion odom_quat2;
    odom_quat2.w = q.w();
    odom_quat2.x = q.x();
    odom_quat2.y = q.y();
    odom_quat2.z = q.z();

    odom_trans2.transform.rotation = odom_quat2;

    odom_broadcaster->sendTransform(odom_trans2);
  }
  */
  //printf("estimated pose: %f, %f, %f\n",twc.at<float>(0), twc.at<float>(1),twc.at<float>(2));

  //CV View/////////////////////////////////////////////////////////////////////
  std::vector<std::shared_ptr<orbslam_wr::FeaturePoint3D>>& key_points
    = frame->feature_points();

  cvtColor(mat,mat,CV_GRAY2BGR);

  for (int i = 0; i < key_points.size(); i++){
    if (!feature_points[i]->pos_world.empty()){
      cv::circle(mat, key_points[i]->keypoint.pt, 2, cv::Scalar(255, 0, 0), -1);
    }
    else {
      cv::circle(mat, key_points[i]->keypoint.pt, 5, cv::Scalar(0, 255, 0), -1);
    }
  }

  cv::imshow("testWindow", mat);
  cv::waitKey(1);

  //std::chrono::time_point<std::chrono::system_clock> foo = std::chrono::system_clock::now();

  //auto difference = std::chrono::duration_cast<std::chrono::milliseconds>(foo - now);
  //printf("diff time: %f hz\n", 1000.0/difference.count());
}

int main(int argc, char** argv) {

  slam_system.LoadSettingsFile("../Realsense.yaml");
  //slam_system.LoadORBVocabularyFile("../ORBvoc.txt");
  slam_system.InitializeSystem();

  ros::init(argc, argv, "ros_main");
  ros::start();

  ros::NodeHandle nh;

  odom_broadcaster = new tf::TransformBroadcaster();

  message_filters::Subscriber<sensor_msgs::Image>
    rgb_sub(nh, "/camera/rgb/image_mono", 1);
  message_filters::Subscriber<sensor_msgs::Image>
    depth_sub(nh, "/camera/depth/image_raw", 1);

  pub = nh.advertise<sensor_msgs::PointCloud>("map_points", 100);

  typedef message_filters::sync_policies
    ::ApproximateTime<sensor_msgs::Image, sensor_msgs::Image> sync_pol;

  message_filters::Synchronizer<sync_pol> sync(sync_pol(10), rgb_sub, depth_sub);
  sync.registerCallback(&Callback);

  cv::namedWindow("testWindow", CV_WINDOW_AUTOSIZE);

  ros::spin();

  cv::destroyWindow("testWindow");

  ros::shutdown();

  return 0;
}
